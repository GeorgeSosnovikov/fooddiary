﻿using FoodDiary.Help;
using FoodDiary.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodDiary.ViewModel
{
    class ReportViewModel : INotifyPropertyChanged
    {
        Model1 dbcontext = new Model1();

        public class CaloriesInDay
        {
            public CaloriesInDay(string date, double calories)
            {
                this.date = date;
                this.calories = calories;
            }
            public string date { get; set; }
            public double calories { get; set; }
        }

        private DateTime _selectedTimeStart;
        public DateTime selectedTimeStart
        {
            get
            {
                return _selectedTimeStart;
            }
            set
            {
                if (_selectedTimeStart != value)
                {
                    _selectedTimeStart = value;
                    OnPropertyChanged("selectedTimeStart");
                }
            }
        }

        private DateTime _selectedTimeFinish;
        public DateTime selectedTimeFinish
        {
            get
            {
                return _selectedTimeFinish;
            }
            set
            {
                if (_selectedTimeFinish != value)
                {
                    _selectedTimeFinish = value;
                    OnPropertyChanged("selectedTimeFinish");
                }
            }
        }

        private ObservableCollection<CaloriesInDay> _allCaloriesInDay;
        public ObservableCollection<CaloriesInDay> allCaloriesInDay
        {
            get
            {
                //_allCaloriesInDay = GetAllCaloriesInDay();
                return _allCaloriesInDay;
            }
            set
            {
                if (_allCaloriesInDay != value)
                {
                    _allCaloriesInDay = value;
                    OnPropertyChanged("allCaloriesInDay");
                }
            }
        }

        private RelayCommand _update;
        public RelayCommand update
        {
            get
            {
                return _update ??
                  (_update = new RelayCommand(obj =>
                  {
                      allCaloriesInDay = GetAllCaloriesInDay();
                  }));
            }
        }

        public ReportViewModel()
        {
            selectedTimeStart = DateTime.Now;//Parse("12.12.2019");
            selectedTimeFinish = DateTime.Now;
            //var a = GetAllCaloriesInDay();
        }

        ObservableCollection<CaloriesInDay> GetAllCaloriesInDay()
        {
            ObservableCollection<CaloriesInDay> all = new ObservableCollection<CaloriesInDay>();
            DateTime start = selectedTimeStart;
            DateTime finish = selectedTimeFinish;
            for (DateTime ii11 = start; ii11 <= finish; ii11 = ii11.AddDays(1))
            {
                double sum = 0;
                //получили все приёмы пищи за день
                var res = (from c in dbcontext.Eating
                           where ((c.Time.Day == ii11.Day) && (c.Time.Month == ii11.Month) && (c.Time.Year == ii11.Year))
                           select c).ToList();
                foreach (Eating e in res)
                {
                    var products = (from p in dbcontext.Product
                                    join ep in dbcontext.Eating_Product on p.Id equals ep.Product_FK
                                    where ep.Eating_FK == e.Id
                                    select p);
                    foreach (Product pr in products)
                    {
                        var res1 = (from port in dbcontext.Portion
                                    join ep in dbcontext.Eating_Product on port.Id equals ep.Portion_FK
                                    where ep.Eating_FK == e.Id && ep.Product_FK == pr.Id
                                    select port.Weight).FirstOrDefault();
                        //var weight = (from port in dbcontext.Portion
                        //              where pr.Portion_FK == port.Id
                        //              select port.Weight).FirstOrDefault();
                        sum = sum + pr.Calories * res1 / 100;
                    }
                }
                all.Add(new CaloriesInDay(ii11.Day.ToString() + "." + ii11.Month.ToString() + "." + ii11.Year.ToString(), sum));
            }
            return all;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
