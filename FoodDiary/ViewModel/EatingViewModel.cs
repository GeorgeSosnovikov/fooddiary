﻿using FoodDiary.Model;
using FoodDiary.Objects;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodDiary.ViewModel
{
    class EatingViewModel : INotifyPropertyChanged
    {
        Model1 dbcontext = new Model1();
        public List<Eating_Type> eatingTypeList;
        public List<Eating> eatingList;
        public List<Product> productsInEatingList;

        private Eating_Type _selectedEatingType;
        public Eating_Type selectedEatingType
        {
            get { return _selectedEatingType; }
            set
            {
                if (_selectedEatingType != value)
                {
                    _selectedEatingType = value;
                    OnPropertyChanged("selectedEatingType");
                }
            }
        }

        private ObservableCollection<Eating> _selectedEating;
        public ObservableCollection<Eating> selectedEating
        {
            get
            {
                _selectedEating.Clear();
                DateTime date1 = new DateTime(2019, 12, 12);
                List<Eating> res = (from c in dbcontext.Eating
                           where ((c.Time.Day == date1.Day) && (c.Time.Month == date1.Month) && (c.Time.Year == date1.Year))
                           select c).ToList();
                //_selectedEating = res.ForEach(i => _selectedEating.Add(i));
                _selectedEating = new ObservableCollection<Eating>();
                return _selectedEating;
            }
        }

        private ObservableCollection<Product> _productsInEating;
        public ObservableCollection<Product> productsInEating
        {
            get
            {
                return _productsInEating;
            }
            set
            {
                if (_productsInEating != value)
                {
                    _productsInEating = value;
                    OnPropertyChanged("productsInEating");
                }
            }
        }

        private ObservableCollection<Eating> _allEating;
        public ObservableCollection<Eating> allEating
        {
            get { return _allEating; }
            set
            {
                if (_allEating != value)
                {
                    _allEating = value;
                    OnPropertyChanged("allEating");
                }
            }
        }

        private ObservableCollection<Eating_Type> _allEatingType;
        public ObservableCollection<Eating_Type> allEatingType
        {
            get { return _allEatingType; }
            set
            {
                if (_allEatingType != value)
                {
                    _allEatingType = value;
                    OnPropertyChanged("allEatingType");
                }
            }
        }
        public List<Eating> today;

        //private ObservableCollection<Eating> _allEatingToday;
        //public ObservableCollection<Eating> allEatingToday
        //{
        //    get
        //    {
        //        //_allEatingToday = new ObservableCollection<Eating>();
        //        //_allEatingToday.Clear();
        //        DateTime date1 = new DateTime(2019, 12, 12);
        //        var res = (from c in dbcontext.Eating
        //                   where ((c.Time.Day == date1.Day) && (c.Time.Month == date1.Month) && (c.Time.Year == date1.Year))
        //                   select c).ToList();//.ForEach(i => _allEatingToday.Add(i));
        //        //_allEatingToday = res.ForEach(i => _allEatingToday.Add(i));
        //        _allEatingToday = new ObservableCollection<Eating>(res);
        //        return _allEatingToday;
        //    }
        //    set
        //    {
        //        if (_allEatingToday != value)
        //        {
        //            _allEatingToday = value;
        //            OnPropertyChanged("allEatingToday");
        //        }
        //    }
        //}


        public EatingViewModel()
        {
            EatingObj a = new EatingObj();
            eatingTypeList = dbcontext.Eating_Type.ToList();
            eatingList = dbcontext.Eating.ToList();
            productsInEatingList = dbcontext.Product.ToList();
            today = new List<Eating> { new Eating { Eating_Type_FK = 1} };
           //productsInEatingList = GetProductsInEatingToList();
            allEatingType = new ObservableCollection<Eating_Type>(eatingTypeList);
            allEating = new ObservableCollection<Eating>(eatingList);
            productsInEating = new ObservableCollection<Product>(productsInEatingList);
        }

        List<Product> GetProductsInEatingToList()
        {
            if (selectedEatingType != null)
            {
                //List<Product> products = (from p in dbcontext.Product
                //                          join e in dbcontext.Eating_Product on p.Id equals e.Product_FK
                //                          where e.Eating_FK == selectedEating.Id
                //                          select p).ToList();
                ////_productsInEating = new ObservableCollection<Product>(products);
                //return products;
                return null;
            }
            else
            {
                return null;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
