﻿using FoodDiary.Help;
using FoodDiary.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace FoodDiary.ViewModel
{
    class EditProductViewModel : INotifyPropertyChanged
    {
        Model1 dbcontext;
        Product product;

        public EditProductViewModel(Product pr, Model1 db)
        {
            nameProduct = pr.Name;
            caloriesProduct = pr.Calories;
            proteinProduct = pr.Protein;
            fatProduct = pr.Fat;
            carbohydrateProduct = pr.Carbohydrate;
            product = pr;
            dbcontext = db;
            imageProduct = pr.Image;
        }

        private byte[] _imageProduct;
        public byte[] imageProduct
        {
            get { return _imageProduct; }
            set
            {
                if (_imageProduct != value)
                {
                    _imageProduct = value;
                    OnPropertyChanged("imageProduct");
                }
            }
        }

        private string _nameProduct;
        public string nameProduct
        {
            get { return _nameProduct; }
            set
            {
                if (_nameProduct != value)
                {
                    _nameProduct = value;
                    OnPropertyChanged("nameProduct");
                }
            }
        }

        private double _caloriesProduct;
        public double caloriesProduct
        {
            get { return _caloriesProduct; }
            set
            {
                if (_caloriesProduct != value)
                {
                    _caloriesProduct = value;
                    OnPropertyChanged("caloriesProduct");
                }
            }
        }

        private double _proteinProduct;
        public double proteinProduct
        {
            get { return _proteinProduct; }
            set
            {
                if (_proteinProduct != value)
                {
                    _proteinProduct = value;
                    OnPropertyChanged("proteinProduct");
                }
            }
        }

        private double _fatProduct;
        public double fatProduct
        {
            get { return _fatProduct; }
            set
            {
                if (_fatProduct != value)
                {
                    _fatProduct = value;
                    OnPropertyChanged("fatProduct");
                }
            }
        }

        private double _carbohydrateProduct;
        public double carbohydrateProduct
        {
            get { return _carbohydrateProduct; }
            set
            {
                if (_carbohydrateProduct != value)
                {
                    _carbohydrateProduct = value;
                    OnPropertyChanged("carbohydrateProduct");
                }
            }
        }

        private RelayCommand _updateCommand;
        public RelayCommand updateCommand
        {
            get
            {
                return _updateCommand ??
                  (_updateCommand = new RelayCommand(obj =>
                  {
                      Product c = dbcontext.Product.Find(product.Id);
                      c.Calories = caloriesProduct;
                      c.Protein = proteinProduct;
                      c.Fat = fatProduct;
                      c.Carbohydrate = carbohydrateProduct;
                      dbcontext.SaveChanges();
                      //MessageBox.Show("Изменения были сохранены!");
                  }));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
