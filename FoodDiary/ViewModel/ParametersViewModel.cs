﻿using FoodDiary.Help;
using FoodDiary.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodDiary.ViewModel
{
    class ParametersViewModel : INotifyPropertyChanged
    {
        Model1 dbcontext = new Model1();
        public List<Target> allTargetsList;

        private string _name;
        public string name { get { return _name; } set { _name = value; OnPropertyChanged("name"); } }

        private int _age;
        public int age { get { return _age; } set { _age = value; OnPropertyChanged("age"); } }

        private double _weight;
        public double weight { get { return _weight; } set { _weight = value; OnPropertyChanged("weight"); } }

        private double _height;
        public double height { get { return _height; } set { _height = value; OnPropertyChanged("height"); } }

        private ObservableCollection<Target> _allTargets;
        public ObservableCollection<Target> allTargets
        { get { return _allTargets; } set { _allTargets = value; OnPropertyChanged("allTargets"); } }

        private Target _selectedTarget;
        public Target selectedTarget
        { get { return _selectedTarget; } set { _selectedTarget = value; OnPropertyChanged("selectedTarget"); } }


        private RelayCommand _updateParametersCommand;
        public RelayCommand updateParametersCommand
        {
            get
            {
                return _updateParametersCommand ??
                  (_updateParametersCommand = new RelayCommand(obj =>
                  {
                      Client c = dbcontext.Client.Find(1);
                      c.Name = _name;
                      c.Age = _age;
                      c.Weight = _weight;
                      c.Height = _height;
                      c.Target_FK = _selectedTarget.Id;
                      dbcontext.SaveChanges();
                  }));
            }
        }

        public ParametersViewModel()
        {
            allTargetsList = dbcontext.Target.ToList();
            allTargets = new ObservableCollection<Target>(allTargetsList);

            name = GetName();
            age = GetAge();
            weight = GetWeight();
            height = GetHeight();
            selectedTarget = GetTarget();
        }

        string GetName()
        {
            var res = (from c in dbcontext.Client select c.Name).ToList();
            return res.FirstOrDefault();
        }

        int GetAge()
        {
            var res = (from c in dbcontext.Client select c.Age).ToList();
            return res.FirstOrDefault();
        }

        double GetWeight()
        {
            var res = (from c in dbcontext.Client select c.Weight).ToList();
            return res.FirstOrDefault();
        }

        double GetHeight()
        {
            var res = (from c in dbcontext.Client select c.Height).ToList();
            return res.FirstOrDefault();
        }

        Target GetTarget()
        {
            var res = (from c in dbcontext.Client
                       join t in dbcontext.Target on c.Target_FK equals t.Id
                       where (c.Target_FK == t.Id)
                       select t);
            return res.FirstOrDefault();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
