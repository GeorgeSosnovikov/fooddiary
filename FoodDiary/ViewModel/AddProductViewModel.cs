﻿using FoodDiary.Help;
using FoodDiary.Model;
using FoodDiary.Objects;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodDiary.ViewModel
{
    class AddProductViewModel : INotifyPropertyChanged
    {
        Model1 dbcontext = new Model1();
        public List<Product> allProductsList;
        public List<Portion> allPortionList;

        private Product _selectedProduct;
        public Product selectedProduct
        {
            get { return _selectedProduct; }
            set
            {
                if (_selectedProduct != value)
                {
                    _selectedProduct = value;
                    OnPropertyChanged("selectedProduct");
                }
            }
        }

        private Portion _selectedPortion;
        public Portion selectedPortion
        {
            get { return _selectedPortion; }
            set
            {
                if (_selectedPortion != value)
                {
                    _selectedPortion = value;
                    OnPropertyChanged("selectedPortion");
                }
            }
        }

        private ObservableCollection<Product> _allProducts;
        public ObservableCollection<Product> allProducts
        {
            get { return _allProducts; }
            set
            {
                if (_allProducts != value)
                {
                    _allProducts = value;
                    OnPropertyChanged("allProduct");
                }
            }
        }

        private ObservableCollection<Portion> _allPortion;
        public ObservableCollection<Portion> allPortion
        {
            get { return _allPortion; }
            set
            {
                if (_allPortion != value)
                {
                    _allPortion = value;
                    OnPropertyChanged("allPortion");
                }
            }
        }

        private RelayCommand _addProductInEatingCommand;
        public RelayCommand addProductInEatingCommand
        {
            get
            {
                return _addProductInEatingCommand ??
                  (_addProductInEatingCommand = new RelayCommand(obj =>
                  {
                      double sums = 777;
                      Portion p = new Portion()
                      {
                          Weight = sums
                      };
                      dbcontext.Portion.Add(p);
                      dbcontext.SaveChanges();
                  }));
            }
        }

        public AddProductViewModel()
        {

            allProductsList = dbcontext.Product.ToList();
            allProducts = new ObservableCollection<Product>(allProductsList);
            allPortionList = dbcontext.Portion.ToList();
            allPortion = new ObservableCollection<Portion>(allPortionList);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
