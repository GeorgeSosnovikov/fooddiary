namespace FoodDiary.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Client")]
    public partial class Client
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Client()
        {
            Eating = new HashSet<Eating>();
        }

        public int Id { get; set; }

        public double Weight { get; set; }

        public double Height { get; set; }

        [Required]
        [StringLength(30)]
        public string Name { get; set; }

        public int Target_FK { get; set; }

        public int Age { get; set; }

        public virtual Target Target { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Eating> Eating { get; set; }
    }
}
