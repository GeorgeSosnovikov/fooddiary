namespace FoodDiary.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Eating")]
    public partial class Eating
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Eating()
        {
            Eating_Product = new HashSet<Eating_Product>();
        }

        public int Id { get; set; }

        public int Eating_Type_FK { get; set; }

        public DateTime Time { get; set; }

        public int Client_FK { get; set; }

        public virtual Client Client { get; set; }

        public virtual Eating_Type Eating_Type { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Eating_Product> Eating_Product { get; set; }
    }
}
