namespace FoodDiary.Model
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model1")
        {
        }

        public virtual DbSet<Client> Client { get; set; }
        public virtual DbSet<Eating> Eating { get; set; }
        public virtual DbSet<Eating_Product> Eating_Product { get; set; }
        public virtual DbSet<Eating_Type> Eating_Type { get; set; }
        public virtual DbSet<Portion> Portion { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
        public virtual DbSet<Target> Target { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Client>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .HasMany(e => e.Eating)
                .WithRequired(e => e.Client)
                .HasForeignKey(e => e.Client_FK)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Eating>()
                .HasMany(e => e.Eating_Product)
                .WithRequired(e => e.Eating)
                .HasForeignKey(e => e.Eating_FK)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Eating_Type>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Eating_Type>()
                .HasMany(e => e.Eating)
                .WithRequired(e => e.Eating_Type)
                .HasForeignKey(e => e.Eating_Type_FK)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Portion>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Portion>()
                .HasMany(e => e.Eating_Product)
                .WithRequired(e => e.Portion)
                .HasForeignKey(e => e.Portion_FK)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Product>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Product>()
                .HasMany(e => e.Eating_Product)
                .WithRequired(e => e.Product)
                .HasForeignKey(e => e.Product_FK)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Target>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Target>()
                .HasMany(e => e.Client)
                .WithRequired(e => e.Target)
                .HasForeignKey(e => e.Target_FK)
                .WillCascadeOnDelete(false);
        }
    }
}
