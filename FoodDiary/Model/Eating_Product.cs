namespace FoodDiary.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Eating_Product
    {
        public int Eating_FK { get; set; }

        public int Product_FK { get; set; }

        public int Id { get; set; }

        public int Portion_FK { get; set; }

        public virtual Eating Eating { get; set; }

        public virtual Portion Portion { get; set; }

        public virtual Product Product { get; set; }
    }
}
