﻿using FoodDiary.Help;
using FoodDiary.Model;
using FoodDiary.View;
using FoodDiary.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodDiary.Objects
{
    class EatingObj : INotifyPropertyChanged
    {
        Model1 dbcontext = new Model1();
        
        public List<Product> allProductsList;
        public List<Portion> allPortionList;

        private ObservableCollection<Eating> _allEatingToday;
        public ObservableCollection<Eating> allEatingToday
        {
            get
            {
                _allEatingToday = new ObservableCollection<Eating>();
                DateTime date1 = DateTime.Now;//new DateTime.Now;
                var res = (from c in dbcontext.Eating
                                   where ((c.Time.Day == date1.Day) && (c.Time.Month == date1.Month) && (c.Time.Year == date1.Year))
                                   select c).ToList();
                if (res.Count != 0)
                    _allEatingToday = new ObservableCollection<Eating>(res);
                else
                {
                    var eating_type = (from p in dbcontext.Eating_Type
                                select p).ToList();
                    foreach (Eating_Type et in eating_type)
                    {
                        Eating e = new Eating()
                        {
                            Client_FK = 1,
                            Time = date1,
                            Eating_Type_FK = et.Id
                        };
                        dbcontext.Eating.Add(e);
                        dbcontext.SaveChanges();
                        _allEatingToday.Add(e);
                    }
                }
                return _allEatingToday;
            }
            set
            {
                if (_allEatingToday != value)
                {
                    _allEatingToday = value;
                    OnPropertyChanged("allEatingToday");
                }
            }
        }

        //private ObservableCollection<Eating_Type> _allEatingType;
        //public ObservableCollection<Eating_Type> allEatingType
        //{
        //    get
        //    {
        //        DateTime date1 = new DateTime(2019, 12, 12);
        //        var res = (from c in dbcontext.Eating_Type
        //                    join e in dbcontext.Eating on c.Id equals e.Eating_Type_FK
        //                    where ((e.Time.Day == date1.Day) && (e.Time.Month == date1.Month) && (e.Time.Year == date1.Year))
        //                    select c).ToList();
        //        _allEatingType = new ObservableCollection<Eating_Type>(res);
        //        return _allEatingType;
        //    }
        //    set
        //    {
        //        if (_allEatingType != value)
        //        {
        //            _allEatingType = value;
        //            OnPropertyChanged("allEatingType");
        //        }
        //    }
        //}

        private ObservableCollection<Product> _allProductsInEating;
        public ObservableCollection<Product> allProductsInEating
        {
            get
            {
                return _allProductsInEating;
            }
            set
            {
                if (_allProductsInEating != value)
                {
                    _allProductsInEating = value;
                    OnPropertyChanged("allProductsInEating");
                }
            }
        }

        private Eating _selectedEatingToday;
        public Eating selectedEatingToday
        {
            get
            {
                allProductsInEating = GetProductsInEatingToList();
                totalCaloriesInEating = GetCaloriesInEating();
                return _selectedEatingToday; 
            }
            set
            {
                if (_selectedEatingToday != value)
                {
                    _selectedEatingToday = value;
                    OnPropertyChanged("selectedEatingToday");
                }
            }
        }

        private Product _selectedProductInEating;
        public Product selectedProductInEating
        {
            get
            {
                return _selectedProductInEating;
            }
            set
            {
                if (_selectedProductInEating != value)
                {
                    _selectedProductInEating = value;
                    OnPropertyChanged("selectedEatingToday");
                }
            }
        }

        private Product _selectedProduct;
        public Product selectedProduct
        {
            get { return _selectedProduct; }
            set
            {
                if (_selectedProduct != value)
                {
                    _selectedProduct = value;
                    OnPropertyChanged("selectedProduct");
                }
            }
        }

        private Portion _selectedPortion;
        public Portion selectedPortion
        {
            get { return _selectedPortion; }
            set
            {
                if (_selectedPortion != value)
                {
                    _selectedPortion = value;
                    OnPropertyChanged("selectedPortion");
                }
            }
        }

        private ObservableCollection<Product> _allProducts;
        public ObservableCollection<Product> allProducts
        {
            get { return _allProducts; }
            set
            {
                if (_allProducts != value)
                {
                    _allProducts = value;
                    OnPropertyChanged("allProduct");
                }
            }
        }

        private ObservableCollection<Portion> _allPortion;
        public ObservableCollection<Portion> allPortion
        {
            get { return _allPortion; }
            set
            {
                if (_allPortion != value)
                {
                    _allPortion = value;
                    OnPropertyChanged("allPortion");
                }
            }
        }

        private RelayCommand _addProductInEatingCommand;
        public RelayCommand addProductInEatingCommand
        {
            get
            {
                return _addProductInEatingCommand ??
                  (_addProductInEatingCommand = new RelayCommand(obj =>
                  {
                      Product p = new Product();
                      p = selectedProduct;
                      //p.Portion_FK = selectedPortion.Id;
                      Eating_Product e = new Eating_Product()
                      {
                          Eating_FK = selectedEatingToday.Id,
                          Product_FK = p.Id,
                          Portion_FK = selectedPortion.Id
                      };
                      dbcontext.Eating_Product.Add(e);
                      dbcontext.SaveChanges();
                      selectedProductInEating = (from c in dbcontext.Product where c.Id == e.Product_FK select c).FirstOrDefault();
                      caloriesLeft = GetDailyRateCaloriesLeft();
                  }));
            }
        }

        private RelayCommand _editProductCommand;
        public RelayCommand editProductCommand
        {
            get
            {
                return _editProductCommand ??
                  (_editProductCommand = new RelayCommand(obj =>
                  {
                      if (_selectedProduct != null)
                      {
                          EditProductView editView = new EditProductView();
                          EditProductViewModel vm = new EditProductViewModel(_selectedProduct, dbcontext);
                          editView.DataContext = vm;
                          editView.ShowDialog();
                          allProductsInEating = GetProductsInEatingToList();
                          allProductsList = dbcontext.Product.ToList();
                          allProducts = new ObservableCollection<Product>(allProductsList);
                          caloriesLeft = GetDailyRateCaloriesLeft();
                          totalCaloriesInEating = GetCaloriesInEating();
                          dailyTarget = GetDailyRateCalories();
                      }
                  }));
            }
        }

        private RelayCommand _deleteProductInEatingCommand;
        public RelayCommand deleteProductInEatingCommand
        {
            get
            {
                var res = (from c in dbcontext.Eating_Product
                           where ((c.Eating_FK == selectedEatingToday.Id) &&
                           (c.Product_FK == selectedProductInEating.Id))
                           select c);
                return _deleteProductInEatingCommand ??
                    (_deleteProductInEatingCommand = new RelayCommand(obj =>
                    {
                        dbcontext.Eating_Product.Remove(res.FirstOrDefault());
                        dbcontext.SaveChanges();
                        allProductsInEating = GetProductsInEatingToList();
                        caloriesLeft = GetDailyRateCaloriesLeft();
                    }));
            }
        }
 
        //private double _portionNameFromSelectedProduct;
        //public double portionNameFromSelectedProduct
        //{
        //    get
        //    {
        //        var res = (from po in dbcontext.Portion
        //                   join pr in dbcontext.Product on po.Id equals pr.Portion_FK
        //                   where (pr.Id == selectedProduct.Id)
        //                   select po.Weight);
        //        //_allEatingToday = new ObservableCollection<Eating>(res);
        //        _portionNameFromSelectedProduct = res.FirstOrDefault();
        //        return _portionNameFromSelectedProduct;
        //    }
        //    set
        //    {
        //        if (_portionNameFromSelectedProduct != value)
        //        {
        //            _portionNameFromSelectedProduct = value;
        //            OnPropertyChanged("portionNameFromSelectedProduct");
        //        }
        //    }
        //}

        private double _totalCaloriesInEating;
        public double totalCaloriesInEating
        {
            get
            {
                _totalCaloriesInEating = GetCaloriesInEating();
                return _totalCaloriesInEating;
            }
            set
            {
                if (_totalCaloriesInEating != value)
                {
                    _totalCaloriesInEating = value;
                    OnPropertyChanged("totalCaloriesInEating");
                }
            }
        }

        private double _dailyTarget;
        public double dailyTarget
        {
            get
            {
                _dailyTarget = GetDailyRateCalories();
                return _dailyTarget;
            }
            set
            {
                if (_dailyTarget != value)
                {
                    _dailyTarget = value;
                    OnPropertyChanged("dailyTarget");
                }
            }
        }

        private string _caloriesLeft;
        public string caloriesLeft
        {
            get
            {
                _caloriesLeft = GetDailyRateCaloriesLeft();
                return _caloriesLeft;                   
            }
            set
            {
                if (_caloriesLeft != value)
                {
                    _caloriesLeft = value;
                    OnPropertyChanged("caloriesLeft");
                }
            }
        }

        public EatingObj()
        {
            allProductsList = dbcontext.Product.ToList();
            allProducts = new ObservableCollection<Product>(allProductsList);
            allPortionList = dbcontext.Portion.ToList();
            allPortion = new ObservableCollection<Portion>(allPortionList);
            selectedPortion = allPortion.FirstOrDefault();
        }

        ObservableCollection<Product> GetProductsInEatingToList()
        {
            if (_selectedEatingToday != null)
            {
                var products = (from p in dbcontext.Product
                                join e in dbcontext.Eating_Product on p.Id equals e.Product_FK
                                where e.Eating_FK == _selectedEatingToday.Id
                                select p).ToList();
                var res = new ObservableCollection<Product>(products);
                return res;
            }
            else
            {
                return null;
            }
        }

        double GetCaloriesInEating()
        {
            if (allProductsInEating != null)
            {
                double sum = 0;
                var res1 = (from d in dbcontext.Eating_Product
                            join pr in dbcontext.Product on d.Product_FK equals pr.Id
                            join port in dbcontext.Portion on d.Portion_FK equals port.Id
                            where d.Eating_FK == _selectedEatingToday.Id
                            
                            select (port.Weight / 100 * pr.Calories)).ToList();
                //var res = (from p in allProductsInEating
                //           join e in dbcontext.Portion on p.Portion_FK equals e.Id
                //           select (e.Weight / 100 * p.Calories)).ToList();
                foreach (double p in res1)
                    sum += p;
                return Math.Round(sum, 2);
            }
            else
                return 0;
        }

        double GetDailyRateCalories()
        {
            double cal = 0;
            Client cl;
            var res = (from c in dbcontext.Client
                       where c.Id == 1
                       select c);
            cl = res.FirstOrDefault();
            cal = 10 * cl.Weight + 6.25 * cl.Height - 5 * cl.Age;
            var n = (from t in dbcontext.Target
                        where cl.Target_FK == t.Id
                        select t.Name);
            string name = n.FirstOrDefault();
            if (name.StartsWith("Набор"))
            {
                cal = cal * 1.15;
            }
            else
            {
                if (name.StartsWith("Похудение"))
                {
                    cal = cal / 1.15;
                }
            }
            return Math.Round(cal, 2);
        }

        string GetDailyRateCaloriesLeft()
        {
            double sum = GetDailyRateCalories();
            foreach (Eating e in allEatingToday)
            {
                var products = (from p in dbcontext.Product
                                join ep in dbcontext.Eating_Product on p.Id equals ep.Product_FK
                                where ep.Eating_FK == e.Id
                                select p).ToList();
                foreach (Product pr in products)
                {
                    var res2 = (from ds in dbcontext.Portion
                                join kl in dbcontext.Eating_Product on ds.Id equals kl.Portion_FK
                                where ((kl.Eating_FK == e.Id) && (kl.Product_FK == pr.Id))
                                select ds.Weight).FirstOrDefault();
                    sum = Math.Round(sum - (pr.Calories * res2 / 100), 2);
                }
            }
            if (sum < 0)
                return "перебор на " + Math.Abs(sum).ToString();
            else
                return sum.ToString();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
